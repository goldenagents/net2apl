package org.uu.nl.net2apl.core.fipa.acl;

import java.io.Serializable;

public interface FIPASendableObject extends Serializable {}
